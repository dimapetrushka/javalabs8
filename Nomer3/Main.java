package Nomer3;

import java.util.HashMap;

/**
 * Created by Dima1 on 24.04.2016.
 * Коллекция HashMap из котов Есть класс Cat, с полем имя (name, String).Создать коллекцию HashMap<String, Cat>.
 * Добавить в коллекцию 10 котов, в качестве ключа использовать имя кота. Вывести результат на экран, каждый элемент с новой строки.
 */
public class Main {
    public static void main(String[] args) {
        HashMap<String, Cat> map = new HashMap<String, Cat>();
        Cat cat = new Cat();
        Cat cat1 = new Cat();
        Cat cat2 = new Cat();
        Cat cat3 = new Cat();
        Cat cat4 = new Cat();
        Cat cat5 = new Cat();
        Cat cat6 = new Cat();
        Cat cat7 = new Cat();
        Cat cat8 = new Cat();
        Cat cat9 = new Cat();

        map.put("Дима",cat);
        map.put("Лёша",cat1);
        map.put("Андрей",cat2);
        map.put("Женя",cat3);
        map.put("Макс",cat4);
        map.put("Паша",cat5);
        map.put("Боря",cat6);
        map.put("Петя",cat7);
        map.put("Вася",cat8);
        map.put("Саша",cat9);

        for (HashMap.Entry<String,Cat> para: map.entrySet()) {
            String key = para.getKey();
            Cat value = para.getValue();
            System.out.println(key + " " + value);
        }
    }
}

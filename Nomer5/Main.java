package Nomer5;

import java.util.HashMap;

/**
 * Created by Dima1 on 24.04.2016.
 * Коллекция HashMap из Object Есть коллекция HashMap<String, Object>, туда занесли 10 различных пар объектов.
 * Вывести содержимое коллекции на экран, каждый элемент с новой строки.
 * Пример вывода (тут показана только одна строка): Sim - 5
 */
public class Main {
    public static void main(String[] args) {
        HashMap<String,Object> map = new HashMap<String,Object>();
        Object object = new Object();
        Object object1 = new Object();
        Object object2 = new Object();
        Object object3 = new Object();
        Object object4 = new Object();
        Object object5 = new Object();
        Object object6 = new Object();
        Object object7 = new Object();
        Object object8 = new Object();
        Object object9 = new Object();

        map.put("Один",object);
        map.put("Два",object1);
        map.put("Три",object2);
        map.put("Четыре",object3);
        map.put("Пять",object4);
        map.put("Шесть",object5);
        map.put("Семь",object6);
        map.put("Восемь",object7);
        map.put("Девять",object8);
        map.put("Десять",object9);

        for (HashMap.Entry<String,Object> para: map.entrySet()) {
            String key = para.getKey();
            Object value = para.getValue();
            System.out.println(key + " - " + value);
        }
    }
}

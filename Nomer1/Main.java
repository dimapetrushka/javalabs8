package Nomer1;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Dima1 on 24.04.2016.
 * HashSet из растений Создать коллекцию HashSet с типом элементов String. Добавить в неё 10 строк:
 * арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника, ирис, картофель.
 * Вывести содержимое коллекции на экран, каждый элемент с новой строки.Посмотреть, как изменился порядок добавленных элементов.
 */
public class Main {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<String>();
        set.add("Арбуз");
        set.add("Банан");
        set.add("Вишня");
        set.add("Груша");
        set.add("Дыня");
        set.add("Ежевика");
        set.add("Жень-Шень");
        set.add("Земляника");
        set.add("Ирис");
        set.add("Картофель");

        for (String text : set) {
            System.out.println(text);
        }

    }
}

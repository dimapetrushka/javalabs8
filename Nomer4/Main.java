package Nomer4;

import java.util.HashMap;

/**
 * Created by Dima1 on 24.04.2016.
 * Вывести на экран список ключей Есть коллекция HashMap<String, String>, туда занесли 10 различных строк.
 * Вывести на экран список ключей, каждый элемент с новой строки.
 *
 * Вывести на экран список значений, каждый элемент с новой строки.
 */
public class Main {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Дима", "Петрушка");
        map.put("Лёша", "Чернавский");
        map.put("Андрей", "Фоминых");
        map.put("Женя", "Щелконогов");
        map.put("Макс", "Нурмахометов");
        map.put("Паша", "Стародубцев");
        map.put("Боря", "Витязев");
        map.put("Петя", "Васичкинн");
        map.put("Вася", "Петичкин");
        map.put("Саша", "Сашов");

        for (HashMap.Entry<String, String> para : map.entrySet()) {
            String key = para.getKey();
            System.out.println(key);
        }
        System.out.println();

        for (HashMap.Entry<String, String> para : map.entrySet()) {
            String value = para.getValue();
            System.out.println(value);
        }
    }
}
